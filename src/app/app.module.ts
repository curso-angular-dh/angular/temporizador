import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ProgressBarComponent} from './progress-bar/progress-bar.component';
import {CountdownComponent} from './countdown/countdown.component';
import {FormsModule} from '@angular/forms';
import {TimerComponent} from './timer/timer.component';
import {DisplayComponent} from './display/display.component';
import {TimerService} from './timer/timer.service';
import {TimerNoneComponent} from './timer-none/timer.component';
import {TimerShadowComponent} from './timer-shadow/timer.component';
import { AlertViewComponent } from './alert-view/alert-view.component';

@NgModule({
  declarations: [
    AppComponent,
    ProgressBarComponent,
    CountdownComponent,
    TimerComponent,
    TimerNoneComponent,
    TimerShadowComponent,
    DisplayComponent,
    AlertViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
