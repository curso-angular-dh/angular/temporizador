import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public isAddTimerVisible: boolean;
  public isEndTimerAlertVisible: boolean;

  public time: number;

  public timers: number[];

  constructor() {
    this.isAddTimerVisible = false;
    this.isEndTimerAlertVisible = false;
    this.time = 0;

    this.timers = [3, 20, 185];
  }

  public onFinish(): void {
    console.log('FINISH FROM APP');
  }

  public showAddTimer(): void {
    this.isAddTimerVisible = true;
  }

  public hideAddTimer(): void {
    this.isAddTimerVisible = false;
  }

  public submitAddTimer(): void {
    this.timers.push(this.time);
    this.hideAddTimer();
  }

  public showEndTimerAlert(): void {
    this.isEndTimerAlertVisible = true;
  }

  public hideEndTimerAlert(): void {
    this.isEndTimerAlertVisible = false;
  }
}
