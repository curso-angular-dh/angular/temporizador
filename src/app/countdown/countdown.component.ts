import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy, OnChanges {

  @Output() decrease: EventEmitter<number>;
  @Output() finish: EventEmitter<void>;

  @Input() init: number;

  public counter: number;

  private timeout: number;

  constructor() {
    this.decrease = new EventEmitter<number>();
    this.finish = new EventEmitter<void>();

    this.counter = 0;

    this.timeout = null;
  }

  ngOnInit() {
    this.initialize();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('--------------- value changed: ', changes);

    if (changes.hasOwnProperty('init')) {
      this.initialize();
    }
  }

  ngOnDestroy(): void {
    this._clearTimeout();
  }

  // Inicializar el contador
  public initialize(): void {
    if (this.init && this.init > 0) {
      this._clearTimeout();

      this.counter = this.init;
      this.doCountdown();
    }
  }

  // Ejecutar el Countdown
  public doCountdown(): void {
    this.timeout = setTimeout(() => {
      this.counter--;
      this.validate();
    }, 1000);
  }

  // Control el proceso
  public validate(): void {
    this.decrease.emit(this.counter);
    console.log('count is', this.counter);

    if (this.counter === 0) {
      this.finish.emit();
      console.log('-- counter end --');
    } else {
      this.doCountdown();
    }
  }

  private _clearTimeout(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

}
