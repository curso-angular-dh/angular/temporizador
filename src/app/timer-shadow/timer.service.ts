import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService {

  private timeout: number;

  public paused: boolean;

  public init: number;

  private finishSubject: Subject<void>;
  public finish: Observable<void>;

  private countdownSubject: BehaviorSubject<number>;
  public countdown$: Observable<number>;

  constructor() {

    this.timeout = null;

    this.paused = true;

    this.init = 0;

    this.finishSubject = new Subject<void>();
    this.finish = this.finishSubject.asObservable();

    this.countdownSubject = new BehaviorSubject(0);
    this.countdown$ = this.countdownSubject.asObservable();
  }

  destroy(): void {
    this._clearTimeout();
  }

  public restartValues(init?: number): void {
    if (init) {
      this.init = init;
    }

    if (this.init && this.init > 0) {
      this.paused = true;

      this._clearTimeout();

      this.countdownSubject.next(this.init);
    }
  }

  public toogleCountdown(): void {
    this.paused = !this.paused;

    if (this.paused) {
      this._clearTimeout();
    } else {
      this.doCountdown();
    }
  }

  public doCountdown(): void {
    this.timeout = setTimeout(() => {
      this.countdownSubject.next(this.countdownSubject.getValue() - 1);
      this.validate();
    }, 1000);
  }

  public validate(): void {
    if (this.countdownSubject.getValue() === 0) {
      this.finishSubject.next();
      console.log('-- counter end --');
    } else {
      this.doCountdown();
    }
  }

  private _clearTimeout(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }
}
