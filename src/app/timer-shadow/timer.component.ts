import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {TimerService} from './timer.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-timer-shadow',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  providers: [TimerService],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.ShadowDom
})
export class TimerShadowComponent implements OnInit, OnDestroy {

  @Output() public finish: EventEmitter<void>;

  @Input() public init: number;

  public countdown: number;

  private countdownEndSubscription: Subscription;
  private countdownSubscription: Subscription;

  constructor(public timerService: TimerService,
              private cdRef: ChangeDetectorRef) {
    this.finish = new EventEmitter<void>();

    this.countdown = 0;
  }

  ngOnInit() {
    this.timerService.restartValues(this.init);

    this.countdownEndSubscription = this.timerService.finish.subscribe(
      () => {
        console.log('------ countdown end -----');
        this.finish.emit();
      }
    );

    this.countdownSubscription = this.timerService.countdown$.subscribe(
      (data: number) => {
        this.countdown = data;
        this.cdRef.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this.timerService.destroy();

    this.countdownEndSubscription.unsubscribe();
    this.countdownEndSubscription = null;

    this.countdownSubscription.unsubscribe();
    this.countdownSubscription = null;
  }

  get progress() {
    console.log('Getting progress!!!!');
    return (this.init - this.countdown) * 100 / this.init;
  }
}
